import { Component, ChangeDetectorRef, HostBinding } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { ThemingComponent } from './angular-material/theming/theming.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-dashboard-free';


  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  temaActual: string = localStorage.getItem("tema")
  sesion: string;

  toHighlight: string = ""
  pageScroll: Number = 0;

  location: Location;
  loaderApp: boolean = true;


  @HostBinding('class') activeThemeCssClass: string
  isThemeDark = false
  activeTheme: string
  themes: string[] = [
    'deeppurple-amber',
    'indigo-pink',
    'pink-bluegrey',
    'purple-green',
  ]

  constructor(
    public overlayContainer: OverlayContainer,
    private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private activatedRoute: ActivatedRoute,
    private Theming: ThemingComponent
  ) {
    // document.querySelector("body").classList.add("light")

    // this.setActiveTheme('indigo-pink', /* darkness: */ false)

    // if (localStorage.getItem('tema')) {
    //   this.temaActual = localStorage.getItem("tema")
    // }
    this.mobileQuery = media.matchMedia('(max-width: 998px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    router.events.subscribe((val) => {
      this.sesion = localStorage.getItem("sesion")
    });


  }


  ngOnInit() {

    setTimeout(() => {
      this.loaderApp = false
    }, 2000);
  }

  ngOnDestroy() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }


  // setActiveTheme(theme: string, darkness: boolean = null) {
  //   if (darkness === null)
  //     darkness = this.isThemeDark
  //   else if (this.isThemeDark === darkness) {
  //     if (this.activeTheme === theme) return
  //   } else
  //     this.isThemeDark = darkness

  //   this.activeTheme = theme

  //   const cssClass = darkness === true ? theme + THEME_DARKNESS_SUFFIX : theme

  //   const classList = this.overlayContainer.getContainerElement().classList
  //   if (classList.contains(this.activeThemeCssClass))
  //     classList.replace(this.activeThemeCssClass, cssClass)
  //   else
  //     classList.add(cssClass)

  //   this.activeThemeCssClass = cssClass
  // }

  // toggleDarkness() {
  //   this.setActiveTheme(this.activeTheme, !this.isThemeDark)

  //   if (this.isThemeDark == true) {
  //     document.querySelector("body").classList.add("dark")
  //     document.querySelector("body").classList.remove("light")
  //   } else {
  //     document.querySelector("body").classList.add("light")
  //     document.querySelector("body").classList.remove("dark")
  //   }
  // }

}

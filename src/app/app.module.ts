import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Snackbar } from './utilities/snackbar/snackbar';
import { AdminComponent } from './layouts/admin-layout/admin/admin.component';
import { SidenavComponent } from './layouts/admin-layout/sidenav/sidenav.component';
import { ThemingComponent } from './angular-material/theming/theming.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};



/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////   APP ANIMATIONS   ////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

/*HABILITAR O DESHABILITAR LAS ANIMACIONES DE ANGULAR MATERIAL ( AngularMaterialModule )
CON UNA VARIABLE EN EL localStorage*/

if (!localStorage.getItem("app_animations")) {
	localStorage.setItem("app_animations", "enabled");
}

const animations: string = localStorage.getItem("app_animations");

var AnimationsModule: any = animations == "disabled"
	? NoopAnimationsModule
	: BrowserAnimationsModule;

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////   APP ANIMATIONS   ////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

@NgModule({
	declarations: [
		AppComponent,
		AdminComponent,
		SidenavComponent,
		Snackbar
	],
	imports: [
		BrowserModule,
		AppRoutingModule, /* ANGULAR ROUTING */
		AnimationsModule, /* ANGULAR MATERIAL ANIMATIONS (BrowserAnimationsModule),(NoopAnimationsModule) */
		AngularMaterialModule, /* ANGULAR MATERIAL COMPONENTS IMPORT*/
		PerfectScrollbarModule,
	],
	providers: [
		Snackbar, /* ANGULAR MATERIAL SNACKKBARS MODIFICADOS */
		ThemingComponent,
		,
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
		path: '',
		redirectTo: 'auth/login',
		pathMatch: 'full',
	}, {
		path: 'admin',
		loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
	}, {
		path: 'auth',
		loadChildren: './layouts/login-layout/login-layout.module#LoginLayoutModule'
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Routes } from '@angular/router';
import { HomeComponent } from './admin-layout-components/home/home.component';

export const AdminLayoutRouting: Routes = [
    {
        path: '',
    }, {
        path: 'home',
        component: HomeComponent,
        data: { title: 'Inicio' },
        
    },
    // {
    //     path: 'parent',
    //     children: [
    //         {
    //             path: 'children',
    //             component: childrenComponent,
    //             data: { title: 'children' }
    //         },
    //         {
    //             path: 'children',
    //             component: childrenComponent,
    //             data: { title: 'children' }
    //         },

    //     ]
    // }
];
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ActivationEnd, NavigationEnd, ActivationStart, } from '@angular/router';
import { ThemingComponent } from 'src/app/angular-material/theming/theming.component';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
})
export class AdminComponent implements OnInit {
  isMobile: MediaQueryList;
  RouteTitle: string = "";
  Themes: any = [];
  Dark: boolean = true;
  activeTheme: string = ""

  public type: string = 'component';

  public disabled: boolean = false;

  public config: PerfectScrollbarConfigInterface = {};

  @ViewChild(PerfectScrollbarComponent, { static: false }) componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: false }) directiveRef?: PerfectScrollbarDirective;

  private _isMobileListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private route: ActivatedRoute,
    public theming: ThemingComponent
  ) {
    this.Themes = theming.themes;
    this.isMobile = media.matchMedia('(max-width: 800px)');
    this._isMobileListener = () => changeDetectorRef.detectChanges();
    this.isMobile.addListener(this._isMobileListener);

    theming.getStorageTheme();
  }

  public onScrollEvent(event: any): void {
    console.log(event);
  }


  ngOnInit(): void {

    this.activeTheme = localStorage.getItem("tema");
    localStorage.getItem("Darkness") == "true" ? this.Dark = true : this.Dark = false;
    // console.log( this.route.snapshot.data['title']);
    let path = '';

    this.router.events.subscribe((event) => {
      // console.log(event);

      if (event instanceof ActivationStart) {
        if (event.snapshot.data.title) {
          // console.log(event.snapshot.data.title);
          this.RouteTitle = event.snapshot.data.title
        }
      }

    });

  }

  async setTheme(theme: string) {
    console.log(theme);
    this.activeTheme = theme;
    await this.theming.setActiveTheme(theme, this.Dark);
  }

  async setDarkness() {
    this.Dark ? this.Dark = false : this.Dark = true;
    await this.setTheme(this.activeTheme);
    console.log(this.Dark);
    console.log(this.activeTheme);

  }

  ngOnDestroy(): void {
    this.isMobile.removeListener(this._isMobileListener);
  }

}

/**  Copyright 2019 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */
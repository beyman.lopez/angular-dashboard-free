import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material/angular-material.module';
import { AdminLayoutRouting } from './admin-layout.routing';
import { HomeComponent } from './admin-layout-components/home/home.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRouting),
    AngularMaterialModule, /* ANGULAR MATERIAL COMPONENTS IMPORT*/
  ],
  declarations: [
    HomeComponent,
  ]
})

export class AdminLayoutModule { }

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  static tituloModulo: string = "Dashboard"
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(
    media: MediaMatcher, 
    changeDetectorRef: ChangeDetectorRef
    ) {
    this.mobileQuery = media.matchMedia('(max-width: 998px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  public menuItems: any[];

  ngOnInit() {
    this.menuItems = SidebarMenuItem.filter(menuItem => menuItem);
  }


}

export interface menuItems {
  url?: string;
  title: string;
  icon: string;
  type: string
  children?: menuChildItems[];
}

export interface menuChildItems {
  url: string;
  title: string;
  icon: string;
}

//Menu Items
export const SidebarMenuItem: menuItems[] = [
  {
    url: '/admin/home',
    title: 'Dashboard',
    type: 'module',
    icon: 'dashboard'
  }, {
    icon: 'shop_two',
    title: 'Components',
    type: 'collapse',
    children: [
      { url: '/auth/login', title: 'Child 1', icon: 'apps', },
      { url: '/administracion/mod2', title: 'Child 2', icon: 'done', },
    ]
  }
];
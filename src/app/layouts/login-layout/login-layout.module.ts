import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material/angular-material.module';
import { LoginLayoutRouting } from './login-layout.routing';
import { LoginComponent } from './login-layout-components/login/login.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginLayoutRouting),
    AngularMaterialModule, /* ANGULAR MATERIAL COMPONENTS IMPORT*/
  ],
  declarations: [
    LoginComponent,
  ]
})

export class LoginLayoutModule { }

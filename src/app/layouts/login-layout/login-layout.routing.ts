import { Routes } from '@angular/router';
import { LoginComponent } from './login-layout-components/login/login.component';

export const LoginLayoutRouting: Routes = [

    {
        path: '',
    }, {
        path: 'login',
        component: LoginComponent,
        data: { title: 'Iniciar sesion' }
    },
    // {
    //     path: 'parent',
    //     children: [
    //         {
    //             path: 'children',
    //             component: childrenComponent,
    //             data: { title: 'children' }
    //         },
    //         {
    //             path: 'children',
    //             component: childrenComponent,
    //             data: { title: 'children' }
    //         },

    //     ]
    // }
];
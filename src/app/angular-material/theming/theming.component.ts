import { Component, OnInit, HostBinding, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router, ActivatedRoute } from '@angular/router';
import { OverlayContainer } from '@angular/cdk/overlay';
import { AdminComponent } from 'src/app/layouts/admin-layout/admin/admin.component';
const THEME_DARKNESS_SUFFIX = `-dark`

@Component({
	selector: 'app-theming',
	template: '',
})
export class ThemingComponent implements OnInit {

	mobileQuery: MediaQueryList;
	private _mobileQueryListener: () => void;

	title = 'Angular Dashboard';
	temaActual: string = localStorage.getItem("tema");
	sesion: string;
	sidenav_opened: boolean = true

	toHighlight: string = ""
	pageScroll: Number = 0;

	loaderApp: boolean = true;

	@HostBinding('class') activeThemeCssClass: string
	DarkTheme = false
	activeTheme: string
	activeThemeLoader: string;
	themes: string[] = [
		'deeppurple-amber',
		'indigo-pink',
		'pink-bluegrey',
		'purple-green',
	]

	constructor(
		public overlayContainer: OverlayContainer,
		private router: Router,
		private activatedRoute: ActivatedRoute,
	) {
		this.getStorageTheme();
	}

	ngOnInit() {

	}

	getStorageTheme() {
		var activeTheme, DarkTheme
		if (!localStorage.getItem('tema')) {
			activeTheme = 'indigo-pink';
			DarkTheme = false;
			localStorage.setItem("tema", 'indigo-pink');
			localStorage.setItem("Darkness", "false");
		} else {
			var Darkness = localStorage.getItem("Darkness")
			localStorage.setItem("tema", localStorage.getItem("tema"));
			localStorage.setItem("Darkness", Darkness);

			activeTheme = localStorage.getItem("tema");
			this.activeThemeLoader = localStorage.getItem("tema");
			Darkness == "true" ? DarkTheme = true : DarkTheme = false;
		}


		if (localStorage.getItem("Darkness") == "true") {
			document.querySelector("body").classList.add("dark");
			document.querySelector("body").classList.remove("light")
			document.querySelector("html").classList.add("dark");
			document.querySelector("html").classList.remove("light")
			this.DarkTheme = true
		} else {
			document.querySelector("body").classList.add("light");
			document.querySelector("body").classList.remove("dark")
			document.querySelector("html").classList.add("light");
			document.querySelector("html").classList.remove("dark")
			this.DarkTheme = false;
		}

		this.setActiveTheme(activeTheme, DarkTheme)
	}

	async setActiveTheme(theme: string, darkness: boolean = null) {

		console.log(theme);
		console.log(darkness);

		var Darkness
		darkness == true ? Darkness = "true" : Darkness = "false"
		localStorage.setItem("tema", theme);
		localStorage.setItem("Darkness", Darkness);


		if (darkness === null) {

			darkness = this.DarkTheme
		} else if (this.DarkTheme === darkness) {
			if (this.activeTheme === theme) return
		} else {
			this.DarkTheme = darkness
		}

		this.activeTheme = theme

		const cssClass = darkness === true ? theme + THEME_DARKNESS_SUFFIX : theme

		// console.log(
		// 	cssClass
		// );
		const classList = this.overlayContainer.getContainerElement().classList
		// console.log(classList);

		if (classList.contains(this.activeThemeCssClass)) {
			await classList.replace(this.activeThemeCssClass, cssClass)
			await document.querySelector("app-root").classList.replace(this.activeThemeCssClass, cssClass);
			console.log("replace");
		} else {
			await classList.add(cssClass);
			await document.querySelector("app-root").classList.add(cssClass);
			console.log("add");
		}
		this.activeThemeCssClass = cssClass;
		await this.setBodyClass();
	}

	async toggleDarkness(darkness: boolean, theme: string) {
		console.log(theme);
		console.log(darkness);
		await this.setActiveTheme(theme, darkness);
		await this.setBodyClass();
	}

	setBodyClass() {
		if (this.DarkTheme == true) {
			document.querySelector("body").classList.add("dark")
			document.querySelector("body").classList.remove("light")
			document.querySelector("html").classList.add("dark")
			document.querySelector("html").classList.remove("light")
		} else {
			document.querySelector("body").classList.add("light")
			document.querySelector("body").classList.remove("dark")
			document.querySelector("html").classList.add("light")
			document.querySelector("html").classList.remove("dark")
		}
	}

}
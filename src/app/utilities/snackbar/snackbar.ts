import { Component, Inject } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA, MatSnackBar } from '@angular/material';
const styles = `
div{
    position: relative;
    display: block;
    width: 100%;
    height: 100%;
}
button{
    position: relative;
    float: right;
    display: inline-block;
}
.mat-icon{
    position: relative;
    float: left;
    top: 6px;
    display: inline-block;
}
span{
    position: absolute;
    display: inline-block;
    max-width: calc(100% - 65px);
    padding: 0 10px;
    margin: auto 0 !important;
    top: 0;
    bottom: 0;
    height: fit-content;
}`;
const template1: string = `<div><mat-icon>`
const template2: string = `</mat-icon> <span>{{data}}</span><button mat-icon-button color="`
const template3: string = `"(click)="snackBarRef.dismiss()">`
const template4: string = `</button></div>`
var buttonText: string = `OK`

@Component({
    selector: 'snack-error',
    template: `${template1}error${template2}warn${template3}${buttonText}${template4}`,
    styles: [styles],
})
export class snackError {
    constructor(
        public snackBarRef: MatSnackBarRef<snackError>,
        @Inject(MAT_SNACK_BAR_DATA) public data: any) {
    }
}

@Component({
    selector: 'snack-correcto',
    template: `${template1}check_circle${template2}primary${template3}${buttonText}${template4}`,
    styles: [styles],
})
export class snackCorrect {
    constructor(
        public snackBarRef: MatSnackBarRef<snackCorrect>,
        @Inject(MAT_SNACK_BAR_DATA) public data: any) {
    }
}

@Component({
    selector: 'snackbar',
    template: ``,
})
export class Snackbar {
    constructor(
        private snackBar: MatSnackBar,
    ) { }
    /**
	* @author Beyman
	* @param {String} type (recibe solo dos parametros 'c' correct y 'e' error)
    * @param {String} message ('Hola soy un snackbar')
    * @param {number} button_text  string, texto que aparecerá en el botón ('OK')
    + @param {number} duration (duracion en segundos, por ejemplo 3)
	* @description Abrir un snackbar de tipo error o correct
	*/
    public Open(type: string, message: string, button_text: string, duration: number): void {
        buttonText = button_text;
        let _type;
        const _duration = Number(`${duration}000`);
        type == `c` ? _type = snackCorrect : _type = snackError;
        if (type === `c`) {
            _type = snackCorrect
        } else if (type === `e`) {
            _type = snackError
        } else {
            _type = snackError
            message = `el parametro {String} type (recibe sólo dos parametros 'c' correct y 'e' error)`
            duration = 10000
        }
        this.snackBar.openFromComponent(_type, { data: message, duration: _duration, verticalPosition: "bottom", horizontalPosition: "right", panelClass: "custom-snack" });
    }
}